package com.ml4ai

import com.ml4ai.rabbitmq.wrapper.RabbitMQWrapper

package object rabbitmq {

  implicit def rabbitMQWrapper(rabbitMQ: RabbitMQ) = {
    new RabbitMQWrapper(rabbitMQ)
  }

}
