import { connect } from 'react-redux'

const mapState2Props = (state) => {
    return { ...(state.props) }
}

const mapDispatch2Props = (dispatch) => {
    let proxy = (args) => {
        dispatch(args)
    }
    return { dispatch: proxy }
}

const createContainer = (reactComponent, propsMapping, dispatchMapping) => {
    let pm = mapState2Props
    let dm = mapDispatch2Props
    if (propsMapping) {
        pm = (state) => {
            let pmv = mapState2Props(state)
            let pme = propsMapping(pmv)
            return pme
        }
    }
    if (dispatchMapping) {
        dm = (dispatch) => {
            let dmv = mapDispatch2Props(dispatch)
            let dme = dispatchMapping(dmv)
            return dme
        }
    }
    return connect(pm, dm)(reactComponent)
}

export default createContainer