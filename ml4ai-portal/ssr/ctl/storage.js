export default class Storage {

    set(key, value) {
        if (localStorage) {
            localStorage.setItem(key, JSON.stringify(value))
        }
    }

    get(key) {
        if (localStorage) {
            return JSON.parse(localStorage.getItem(key))
        }
    }

    delete(key){
        if(localStorage){
            localStorage.removeItem(key)
        }
    }
}