const path = require('path')
const fs = require('fs')
const express = require('express')
const ReactSSR = require('react-dom/server')
const app = express()

//const proxyMiddleWare = require('http-proxy-middleware')

import { apiPath } from '../env/config'

const proxyOption = {
    target: apiPath,
    changeOrigoin: true,
    ws: true,
    pathRewrite: { '^/api': '/api' }
}

import React from 'react'
import App from '../pages/app'
import { StaticRouter } from 'react-router'
import { debug } from '../env/config'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from '../ctl/reducer'
import { port } from '../env/config'

let boot = (root) => {

    const render2dom = (store, url, ctx) => {
        return (
            <Provider store={store}>
                <StaticRouter location={url} context={ctx}>
                    <App />
                </StaticRouter>
            </Provider>
        )
    }

    //跨域配置
    app.all('*', function (req, res, next) {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Headers', 'Content-Type')
        res.header('Access-Control-Allow-Methods', '*')
        next()
    })

    //app.use("/api", proxyMiddleWare(proxyOption))

    app.use(express.static(path.join(root, 'dist')))

    app.get(`/*`, (req, res, next) => {
        if (req.url.indexOf(".") < 0) {
            if (debug) {
                console.log('接收到请求', req.url)
            }
            const store = createStore(reducer)
            let filePath = path.join(root, 'dist', 'public', 'index.html')
            let context = {}
            let domString = ReactSSR.renderToString(render2dom(store, req.url, context))
            let taskCount = store.getState()["taskCount"]
            if (debug) {
                console.log("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n-------------------------------------------------------")
                console.log(`获取的任务数量：${taskCount}`)
                console.log("\n---------------------------------")
            }
            if (taskCount) {
                store.subscribe(() => {
                    let count = store.getState()["taskCount"]
                    if (count <= 0) {
                        if (debug) {
                            console.log("请求数据完毕，开始渲染")
                        }
                        const newStore = createStore(reducer, store.getState())
                        fs.readFile(filePath, 'utf8', (err, indexContent) => {
                            let render = new Promise((resolve, reject) => {
                                const context = {}
                                const content = ReactSSR.renderToString(render2dom(newStore, req.url, context))
                                const html = indexContent.replace('<!--content-->', content)
                                resolve(html)
                            })
                            render.then((html) => {
                                let text = html.replace("\"{\\\"props\\\":{}}\"", JSON.stringify(JSON.stringify(newStore.getState())))
                                res.end(text)
                            }).catch((reason) => {
                                if (debug) {
                                    console.log("发生错误", reason)
                                    res.end(reason)
                                } else {
                                    res.download(path.join(root, 'dist', 'assets/html/500.html'))
                                }
                            })
                        })
                    }
                })
            } else {
                fs.readFile(filePath, 'utf8', (err, indexContent) => {
                    let render = new Promise((resolve, reject) => {
                        const content = domString
                        const html = indexContent.replace('<!--content-->', content)
                        resolve(html)
                    })
                    render.then((html) => {
                        let text = html.replace("\"{\\\"props\\\":{}}\"", JSON.stringify(JSON.stringify(store.getState())))
                        res.end(text)
                    }).catch((reason) => {
                        if (debug) {
                            console.log("发生错误", reason)
                            res.end(reason)
                        } else {
                            res.download(path.join(root, 'dist', 'assets/html/500.html'))
                        }
                    })
                })
            }
        }
    })

    app.listen(port, function () {
        console.log(`成功的开启服务，端口号：${port}`)
    })
}

export default boot