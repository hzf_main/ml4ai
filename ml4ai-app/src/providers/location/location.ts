import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NetProvider } from '../net/net';
import { GlobalsProvider } from '../globals/globals';

/*
  Generated class for the LocationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationProvider {

  constructor(private net: NetProvider, private globals: GlobalsProvider) {

  }

  public getCurrentPoint(callback: any) {
    let point = { x: 131, y: 31 };
    if (callback) {
      callback(point);
    }
  }

  public getDistance(point: object, callback: any) {
    let me = this;
    if (point) {
      if (callback) {
        me.net.postJson(me.globals.urls.mapgetdistance, point, (response) => {
          if (response && response.code && response.code == "200") {
            callback(response.data);
          } else {

          }
        }, (xhr) => {

        });
      }
    }
  }

}
