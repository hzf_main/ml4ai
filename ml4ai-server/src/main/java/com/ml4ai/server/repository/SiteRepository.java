package com.ml4ai.server.repository;

import com.ml4ai.server.domain.Site;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uesr on 2018/9/10.
 */
public interface SiteRepository extends JpaRepository<Site, Long> {
}
