package com.ml4ai.server.repository;

import com.ml4ai.server.domain.Text;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by leecheng on 2018/10/28.
 */
public interface TextRepo extends JpaRepository<Text, Long> {
}
