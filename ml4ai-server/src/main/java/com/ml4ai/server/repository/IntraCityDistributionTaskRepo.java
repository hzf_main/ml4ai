package com.ml4ai.server.repository;

import com.ml4ai.server.domain.IntraCityDistributionTask;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uesr on 2018/9/12.
 */
public interface IntraCityDistributionTaskRepo extends JpaRepository<IntraCityDistributionTask, Long> {
}
