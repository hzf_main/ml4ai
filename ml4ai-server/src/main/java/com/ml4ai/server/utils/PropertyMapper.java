package com.ml4ai.server.utils;

import java.util.function.Function;

/**
 * Created by leecheng on 2017/11/13.
 */
public class PropertyMapper<T, R> {

    private Function<T, R> function;

    private String source;

    private String target;

    public PropertyMapper(String source, Function<T, R> function) {
        this(source, source, function);
    }

    public PropertyMapper(String source, String target, Function<T, R> function) {
        this.source = source;
        this.target = target;
        this.function = function;
    }

    public PropertyMapper(String source) {
        this(source, source);
    }

    public PropertyMapper(String source, String target) {
        this(source, target, null);
    }

    public R map(T t) {
        if (t == null) return null;
        if (function == null) return (R) t;
        return function.apply(t);
    }

    public String getSource() {
        return this.source;
    }

    public String getTarget() {
        return target;
    }
}
