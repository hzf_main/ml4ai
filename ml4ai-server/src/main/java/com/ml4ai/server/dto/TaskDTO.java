package com.ml4ai.server.dto;

import com.ml4ai.server.domain.base.BusinessType;
import com.ml4ai.server.domain.base.ProcessStatus;
import com.ml4ai.server.dto.base.BaseAuditDTO;
import com.ml4ai.server.utils.annotation.QueryColumn;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by uesr on 2018/9/9.
 */
@Data
public class TaskDTO extends BaseAuditDTO {

    //业务编号
    @QueryColumn
    private String businessCode;

    //业务类型
    @QueryColumn
    private BusinessType businessType;

    //业务标题
    @QueryColumn(queryOper = "LIKE")
    private String businessTitle;

    @NotNull
    private Double amount;

    //业务发布坐标经度
    private Double locationX;

    //业务发布坐标纬度
    private Double locationY;

    //业务的人id
    @QueryColumn(propName = "starter.id")
    private Long starterId;

    //业务发布人
    private UserDTO starter;

    //业务发布时间
    private Long startTime;

    //业务持有人id
    @QueryColumn(propName = "taker.id")
    private Long takerId;

    //业务持有人
    private UserDTO taker;

    //业务接单时间
    private Long takeTime;

    //业务状态
    @QueryColumn
    private ProcessStatus processStatus;

    @QueryColumn(propName = "site.id")
    private Long siteId;

    private SiteDTO site;

    private Double distance4me;
}
