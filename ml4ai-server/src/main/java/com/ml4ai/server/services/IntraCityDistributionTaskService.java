package com.ml4ai.server.services;

import com.ml4ai.server.domain.IntraCityDistributionTask;
import com.ml4ai.server.dto.IntraCityDistributionTaskDTO;
import com.ml4ai.server.services.base.BaseService;

/**
 * Created by uesr on 2018/9/12.
 */
public interface IntraCityDistributionTaskService extends BaseService<IntraCityDistributionTask, IntraCityDistributionTaskDTO> {
}
