package com.ml4ai.server.services.impl;

import com.ml4ai.server.domain.CrawlTask;
import com.ml4ai.server.dto.CrawlTaskDTO;
import com.ml4ai.server.repository.CrawlTaskRepo;
import com.ml4ai.server.services.CrawlTaskService;
import com.ml4ai.server.services.base.impl.BaseServiceImpl;
import com.ml4ai.server.services.mappers.CrawlTaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;

/**
 * Created by leecheng on 2018/10/28.
 */
@Service
@Transactional
public class CrawlTaskServiceImpl extends BaseServiceImpl<CrawlTask, CrawlTaskDTO> implements CrawlTaskService {

    @Autowired
    CrawlTaskRepo crawlTaskRepo;

    @Autowired
    CrawlTaskMapper crawlTaskMapper;

    @Override
    public JpaRepository<CrawlTask, Long> getRepository() {
        return crawlTaskRepo;
    }

    @Override
    public Function<CrawlTask, CrawlTaskDTO> getConvertEntity2DTOFunction() {
        return crawlTaskMapper::e2d;
    }

    @Override
    public Function<CrawlTaskDTO, CrawlTask> getConvertDTO2EntityFunction() {
        return crawlTaskMapper::d2e;
    }
}
