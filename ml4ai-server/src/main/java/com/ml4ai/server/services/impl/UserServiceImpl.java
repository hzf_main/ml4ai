package com.ml4ai.server.services.impl;

import com.ml4ai.server.domain.Menu;
import com.ml4ai.server.domain.User;
import com.ml4ai.server.dto.MenuDTO;
import com.ml4ai.server.dto.UserDTO;
import com.ml4ai.server.repository.MenuRepository;
import com.ml4ai.server.repository.UserRepo;
import com.ml4ai.server.services.UserService;
import com.ml4ai.server.services.base.impl.BaseServiceImpl;
import com.ml4ai.server.services.mappers.MenuMapper;
import com.ml4ai.server.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by uesr on 2018/9/12.
 */
@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User, UserDTO> implements UserService {

    @Autowired
    UserRepo userRepo;

    @Autowired
    UserMapper userMapper;

    @Autowired
    MenuMapper menuMapper;

    @Autowired
    MenuRepository menuRepository;

    @Override
    public Function<User, UserDTO> getConvertEntity2DTOFunction() {
        return userMapper::e2d;
    }

    @Override
    public Function<UserDTO, User> getConvertDTO2EntityFunction() {
        return userMapper::d2e;
    }

    @Override
    public JpaRepository<User, Long> getRepository() {
        return userRepo;
    }

    @Override
    public UserDTO findByLogin(String username) {
        return getConvertEntity2DTOFunction().apply(userRepo.findByLoginAndStatus(username, "1"));
    }

    @Override
    public List<MenuDTO> findByUserId(Long userId) {
        List<Menu> menus = userRepo.getOne(userId).getRoles().stream().flatMap(role -> role.getMenus().stream()).collect(Collectors.toList());
        List<Menu> all = menus.stream().flatMap(menu -> {
            List<Menu> ms = new LinkedList<>();
            Menu c = menu;
            while (c != null) {
                ms.add(c);
                c = c.getParent();
            }
            return ms.stream();
        }).sorted(Comparator.comparingInt(a -> a.getSortNo())).collect(Collectors.toList());
        List<Menu> top = all.stream().filter(menu -> menu.getParent() == null).collect(Collectors.toList());
        return fillChildren(top, all);
    }

    private List<MenuDTO> fillChildren(List<Menu> menus, List<Menu> all) {
        List<MenuDTO> menuDTOS = new LinkedList<>();
        for (int i = 0; i < menus.size(); i++) {
            Menu menu = menus.get(i);
            MenuDTO menuDTO = menuMapper.e2d(menus.get(i));
            List<Menu> children = all.stream().filter(m -> m.getParent() != null && m.getParent().getId().equals(menu.getId())).collect(Collectors.toList());
            menuDTO.setChildren(fillChildren(children, all));
        }
        return menuDTOS;
    }
}
