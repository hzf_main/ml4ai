package com.ml4ai.server.services.mappers;

import com.ml4ai.server.consts.Const;
import com.ml4ai.server.domain.User;
import com.ml4ai.server.dto.UserDTO;
import com.ml4ai.server.repository.UserRepo;
import com.ml4ai.server.utils.Bean2Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by uesr on 2018/9/9.
 */
@Component
public class UserMapper extends BaseMapper<User, UserDTO> {

    @Autowired
    UserRepo userRepo;

    @Override
    public User newEntity() {
        return new User();
    }

    @Override
    public UserDTO newDTO() {
        return new UserDTO();
    }

    @Override
    public User find(Long id) {
        return userRepo.getOne(id);
    }

    @Override
    public void entity2dto(User entity, UserDTO dto) {
        new Bean2Bean().addExcludeProp(Const.getNotCopyAuditFields()).copyProperties(entity, dto);
    }

    @Override
    public void dto2entity(UserDTO dto, User entity) {
        new Bean2Bean().addExcludeProp(Const.getNotCopyAuditFields()).copyProperties(dto, entity);
    }
}
