package com.ml4ai.server.services.impl;

import com.ml4ai.server.domain.Type;
import com.ml4ai.server.dto.TypeDTO;
import com.ml4ai.server.repository.TypeRepository;
import com.ml4ai.server.services.TypeService;
import com.ml4ai.server.services.base.impl.BaseServiceImpl;
import com.ml4ai.server.services.mappers.TypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;

/**
 * Created by uesr on 2018/9/12.
 */
@Service
@Transactional
public class TypeServiceImpl extends BaseServiceImpl<Type, TypeDTO> implements TypeService {

    @Autowired
    TypeMapper typeMapper;

    @Autowired
    TypeRepository typeRepository;

    @Override
    public JpaRepository<Type, Long> getRepository() {
        return typeRepository;
    }

    @Override
    public Function<Type, TypeDTO> getConvertEntity2DTOFunction() {
        return typeMapper::e2d;
    }

    @Override
    public Function<TypeDTO, Type> getConvertDTO2EntityFunction() {
        return typeMapper::d2e;
    }
}
