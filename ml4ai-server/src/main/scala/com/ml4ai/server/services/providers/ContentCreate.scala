package com.ml4ai.server.services.providers

import java.time.Clock
import java.util

import com.google.gson.Gson
import com.ml4ai.server.consts.UserConstants
import com.ml4ai.server.services.{DbMapService, EsService, UserService}
import com.ml4ai.server.services.capability._
import com.ml4ai.server.utils.RestUtil
import com.ml4ai.common.Toolkit
import com.ml4ai.common.Toolkit.StringHelper
import org.springframework.stereotype.Component
import org.springframework.beans.factory.annotation.{Autowired, Value}

@Component
class ContentCreate extends EnhanceCommandReceiver {

  @Value("${app.content.index}")
  var index: String = null

  @Autowired
  var esService: EsService = null

  @Autowired
  var dbMapService: DbMapService = null

  @Autowired
  var userService: UserService = null

  override def post(command: Command): String = {
    val id: String = StringHelper.uuid
    val token = command("token")
    val user = dbMapService.generateLocalMapWrapper(UserConstants.TOKEN_USER_MAP)
    if (user.containsKey(token)) {
      val username = user.get(token)
      val userDTO = userService.findByLogin(username)
      val map: java.util.Map[String, AnyRef] = new util.LinkedHashMap[String, AnyRef]()
      map.put("title", command("title"))
      map.put("nick", userDTO.getNick)
      map.put("date", Toolkit.DateUtils.format(Clock.systemUTC.millis, "yyyy-MM-dd"))
      map.put("content", command("content"))
      if (esService.putJson(index, "default", id, map))
        new Gson().toJson(RestUtil.success())
      else
        new Gson().toJson(RestUtil.failure())
    } else
      new Gson().toJson(RestUtil.unAuthorized())
  }

  override def getSupport(): String = "content.create"

}
