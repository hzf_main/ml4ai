package com.ml4ai.server.services.actors

import akka.actor.ActorSystem

object RootActors {

  val root = ActorSystem("root")

}
