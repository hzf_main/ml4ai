package com.ml4ai.core.test;

import com.ml4ai.core.IsolationExecution;
import com.ml4ai.core.test.vo.TVO;
import org.junit.Test;

import java.net.URL;

public class IsolateClassInvokeExecutionTest {

    @Test
    public void testIsolation() throws Exception {
        TVO.setValue("Hello");
        IsolationExecution isolationExecution = new IsolationExecution(new URL[]{this.getClass().getResource("/")});
        isolationExecution.execute("com.ml4ai.core.test.vo.TVO", "setValue", "你好");
        isolationExecution.execute("com.ml4ai.core.test.vo.ScopeOperator", "main");
        System.out.println(TVO.getValue());
    }

}
