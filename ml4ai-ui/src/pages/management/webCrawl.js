import React from 'react';
import { Menu, Icon } from 'antd';
import { West, Horizontal, Center } from '../common/layout';
import { AjaxListTable } from '../common/display';
import { api } from '../../config.service';

const Component = React.Component;
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

export default class Crawl extends Component {

    constructor(props) {
        super(props);
        this.state = {
            content: undefined
        };
    }

    menuPush(me, it) {
        let key = it.key;
        switch (key) {
            case "crawl":
                me.setState({
                    content: <TaskPage />
                });
                break;
        }
    }

    render() {
        return (<div style={{ position: 'relative', width: '100%', height: '100%' }}>
            <Horizontal>
                <West style={{ borderRight: '1px solid #ccc' }} width={300}>
                    <Menu mode="horizontal" onClick={(menu) => { this.menuPush(this, menu); }} style={{ width: "100%" }}>
                        <Menu.Item key="crawl">
                            <Icon type="mail" />资源抓取
                        </Menu.Item>
                        <Menu.Item key="app">
                            <Icon type="appstore" />浏览资源(未定)
                        </Menu.Item>
                        <SubMenu title={<span className="submenu-title-wrapper"><Icon type="setting" />资源在线</span>}>
                            <MenuItemGroup title="资料">
                                <Menu.Item key="s1">中国新闻网(未定)</Menu.Item>
                                <Menu.Item key="s2">中国网友网(未定)</Menu.Item>
                            </MenuItemGroup>
                            <MenuItemGroup title="拼材">
                                <Menu.Item key="s3">IT网(未定)</Menu.Item>
                                <Menu.Item key="s4">图片素材(未定)</Menu.Item>
                            </MenuItemGroup>
                        </SubMenu>
                    </Menu>
                </West>
                <Center>
                    {this.state.content}
                </Center>
            </Horizontal>
        </div>);
    }

}

class TaskPage extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let taskOperators = [
            { fun: (record) => { }, title: "编辑" },
            { url: api.crawldatastart, callback: (comp, resp) => { }, title: "开始" }
        ];
        return (
            <div className="width100height100">
                <AjaxListTable url={api.crawldata} path="data" translate={{ id: "标识", taskName: "任务名称" }} exclude={["class"]} operators={taskOperators} />
            </div>
        )
    }

}